Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Source: https://metacpan.org/release/Rex
Upstream-Contact: Jan Gehring <jfried@rexify.org>
Upstream-Name: Rex

Files: *
Copyright: 2025, Jan Gehring <jfried@rexify.org>
License: Apache-2.0

Files: lib/Rex/Inventory/HP/ACU.pm
Copyright: 2017, Jan Gehring <jfried@rexify.org>
           2010, Jeremy Cole
License: Artistic or GPL-1+

Files: lib/Rex/Pkg/OpenWrt.pm
       lib/Rex/Cloud/OpenStack.pm
       lib/Rex/Test/Base/has_dir.pm
       lib/Rex/Interface/Shell/Idrac.pm
Copyright: 2013, Ferenc Erki <erkiferenc@gmail.com>
License: Apache-2.0

Files: lib/Rex/Test/Base/has_stat.pm
Copyright: 2016, Robert Abraham <robert@adeven.com>
License: Apache-2.0

Files: lib/Rex/Group/Lookup/Command.pm
Copyright: 2016, xiahou feng <fanyeren@gmail.com>
License: Apache-2.0

Files: lib/Rex/Group/Lookup/XML.pm
       lib/Rex/Output/Base.pm
Copyright: 2016, Nathan Abu <aloha2004@gmail.com>
License: Apache-2.0

Files: lib/Rex/Group/Lookup/YAML.pm
       lib/Rex/Group/Lookup/DBI.pm
Copyright: 2016, Jean-Marie RENOUARD <jmrenouard@gmail.com>
License: Apache-2.0

Files: lib/Rex/Pkg/Arch.pm
Copyright: 2016, Harm Müller <harm _DOT_ mueller _AT_ g m a i l _Dot_ com>
License: Apache-2.0

Files: lib/Rex/Resource/firewall/Provider/ufw.pm
Copyright: 2016, Andrew Beverley
License: Apache-2.0

Files: lib/Rex/Virtualization/Lxc*
Copyright: Oleg Hardt <litwol@litwol.com>
License: Apache-2.0

Files: debian/*
Copyright: Alex Mestiashvili <mestia@debian.org>
           Eric Johnson <github@iijo.org>
License: Apache-2.0 or Artistic or GPL-1+
Comment: Eric Johnson <github@iijo.org> is the author of rex.bash-completion

License: Apache-2.0
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
     https://www.apache.org/licenses/LICENSE-2.0
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 .
 On Debian systems, the complete text of the Apache License,
 Version 2.0 can be found in `/usr/share/common-licenses/Apache-2.0'.

License: Artistic
 This program is free software; you can redistribute it and/or modify
 it under the terms of the Artistic License, which comes with Perl.
 .
 On Debian systems, the complete text of the Artistic License can be
 found in `/usr/share/common-licenses/Artistic'.

License: GPL-1+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 1, or (at your option)
 any later version.
 .
 On Debian systems, the complete text of version 1 of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-1'.
